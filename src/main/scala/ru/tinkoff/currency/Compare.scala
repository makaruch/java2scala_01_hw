package ru.tinkoff.currency

import ru.tinkoff.currency.CompareResult.Greater
import ru.tinkoff.currency.Currency.{Rub, Usd}

// TODO: Необходимо реализовать сравнения для валют по их имени
// 1. Сравнение по названию валют
// 2. Сравнение валют по курсу валют в рублях. Курс нужно брать из Rate[A, B]
// PS: Подумайте хорошо над тем, что должно быть под ???
trait Compare[???] {
  def compareWith(left: ???, right: ???): CompareResult
}

sealed trait CompareResult
object CompareResult {
  case object Less extends CompareResult
  case object Greater extends CompareResult
  case object Equals extends CompareResult
}

//TODO
object CompareApp extends App {

//  Usd.compareWith(Rub) == Greater // should be true

}
